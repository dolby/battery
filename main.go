package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
)

type battery struct {
	name     string
	capacity int
}

type batteries []battery

func main() {
	b := batteries{}

	batteryPaths := getBatteryDirs()
	for _, path := range batteryPaths {
		bat := battery{name: filepath.Base(path), capacity: getCapacity(path)}
		b = append(b, bat)
	}

	b.print()
}

func getBatteryDirs() []string {
	wslBatteryDir := "/sys/class/power_supply/battery"
	if _, err := os.Stat(wslBatteryDir); !os.IsNotExist(err) {
		return []string{wslBatteryDir}
	}

	b, err := filepath.Glob("/sys/class/power_supply/BAT*")
	if err != nil {
		fmt.Println("ERROR: ", err)
		os.Exit(1)
	}
	return b
}

func getCapacity(b string) int {
	c, err := ioutil.ReadFile(path.Join(b, "capacity"))
	if err != nil {
		fmt.Println("ERROR: ", err)
		os.Exit(1)
	}

	i, err := strconv.Atoi(strings.TrimSpace(string(c)))
	if err != nil {
		fmt.Println("ERROR: ", err)
		os.Exit(1)
	}

	return i
}

func (b batteries) print() {
	out := make([]string, len(b))
	for i, battery := range b {
		out[i] = fmt.Sprintf("B%d:%3d%%", i, battery.capacity)
	}

	fmt.Print(strings.Join(out, " "))
}
