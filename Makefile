BINARY = battery
DESTDIR = /usr/local
SOURCE := $(wildcard *.go)

default: ${BINARY}

${BINARY}:
	go build -o ${BINARY}

clean:
	rm -f ${BINARY}

install: ${BINARY}
	mkdir -p ${DESTDIR}/bin
	cp -f ${BINARY} ${DESTDIR}/bin
	chmod 755 ${DESTDIR}/bin/${BINARY}

uninstall:
	rm -f ${DESTDIR}/bin/${BINARY}

.PHONY: clean install uninstall