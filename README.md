# battery
Prints out the battery status for Linux system. Used by tmux status bar.
```
B0:96% B1:99%
```
